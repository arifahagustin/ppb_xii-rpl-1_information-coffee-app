import 'package:flutter/material.dart';
import 'package:coffe_shop/detail.page.dart'; // Perbaikan pada penamaan file
import 'package:coffe_shop/home_page.dart'; // Perbaikan pada penamaan file
import 'package:coffe_shop/login_google.dart'; // Perbaikan pada penamaan file

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LoginGooglePage(),
      theme: ThemeData(
        brightness: Brightness.light,
        primarySwatch: Colors.amber,
      ),
    );
  }
}
