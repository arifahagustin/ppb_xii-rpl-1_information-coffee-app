import 'package:coffe_shop/widget/category.dart';
import 'package:coffe_shop/widget/coffeeshop.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'detail.page.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required String userEmail}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg1.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                Container(
                  height: 140,
                  width: double.infinity,
                  color: Color.fromRGBO(243, 213, 185, 0.8),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Container(
                                alignment: Alignment.topLeft,
                                height: 45,
                                width: 45,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image:
                                        AssetImage("assets/images/mark.jpeg"),
                                  ),
                                  borderRadius: BorderRadius.circular(25),
                                  border: Border.all(
                                    color: Colors.white,
                                    style: BorderStyle.solid,
                                    width: 2,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                "Mark Lee",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontFamily: 'Inder',
                                  fontSize: 20,
                                ),
                              )
                            ],
                          ),
                          Container(
                            alignment: Alignment.topRight,
                            child: Icon(
                              Icons.notifications_active,
                              color: Colors.white,
                              size: 30,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15),
                      child: Container(
                        height: 60,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: Color(0xFFF5F5F7),
                          borderRadius: BorderRadius.circular(30),
                        ),
                        child: TextField(
                          cursorHeight: 20,
                          autofocus: false,
                          decoration: InputDecoration(
                            hintText: "Cari toko Kopi Favoritmu",
                            prefixIcon: Icon(Icons.search),
                            border: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.grey,
                                width: 2,
                              ),
                              borderRadius: BorderRadius.circular(30),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: Category(
                      imagePath: "assets/images/expreso.png",
                      title: "Expreso",
                      borderColor: Colors.black,
                      backgroundColor: Colors.white,
                      textBaseline:
                          TextBaseline.alphabetic, // Menggeser teks ke atas
                    ),
                  ),
                  SizedBox(width: 10), // Spasi antar kotak
                  Expanded(
                    child: Category(
                      imagePath: "assets/images/latte.png",
                      title: "Latte",
                      borderColor: Colors.black,
                      backgroundColor: Colors.white,
                      textBaseline:
                          TextBaseline.alphabetic, // Menggeser teks ke atas
                    ),
                  ),
                  SizedBox(width: 10), // Spasi antar kotak
                  Expanded(
                    child: Category(
                      imagePath: "assets/images/tubruk.png",
                      title: "Tubruk",
                      borderColor: Colors.black,
                      backgroundColor: Colors.white,
                      textBaseline:
                          TextBaseline.alphabetic, // Menggeser teks ke atas
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: ListView(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: Text(
                      "Tempat Kopi Favorit",
                      style: GoogleFonts.montserrat(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => DetailPage(
                            coffeeShopName: "Aju Nice Kopi",
                          ),
                        ),
                      );
                    },
                    child: Container(
                      width: 500,
                      height: 250,
                      child: Card(
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: CoffeeShop(
                          imagePath: "assets/images/kopi.png",
                          nameShop: "Aju Nice Kopi",
                          style: TextStyle(
                            fontFamily: 'Inder',
                            fontWeight: FontWeight.normal,
                          ),
                          jambuka: "06.00-18.00",
                          onTap: () {},
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Color.fromRGBO(243, 213, 185, 0.8),
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: "Beranda"),
          BottomNavigationBarItem(
              icon: Icon(Icons.bookmark), label: "Disimpan"),
          BottomNavigationBarItem(icon: Icon(Icons.person), label: "Profil")
        ],
      ),
    );
  }
}
