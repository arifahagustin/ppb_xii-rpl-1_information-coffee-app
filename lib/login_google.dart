import 'dart:async';
import 'package:coffe_shop/home_page.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginGooglePage extends StatefulWidget {
  const LoginGooglePage({Key? key}) : super(key: key);

  @override
  _LoginGooglePageState createState() => _LoginGooglePageState();
}

class _LoginGooglePageState extends State<LoginGooglePage> {
  TextEditingController passwordController = TextEditingController();
  TextEditingController nameController = TextEditingController();

  String userEmail = "";
  bool showSplashScreen = true;

  @override
  void initState() {
    super.initState();
    // Tambahkan delay untuk menampilkan splash screen sebelum tampilan login
    Timer(Duration(seconds: 3), () {
      setState(() {
        showSplashScreen = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: !showSplashScreen
          ? AppBar(
              title: Text(
                "Login",
                style: TextStyle(
                  fontFamily: 'InriaSans',
                ),
              ),
              backgroundColor: Color.fromRGBO(243, 213, 185, 0.8),
              actions: [
                GestureDetector(
                  onTap: () {},
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 16),
                    child: Image.asset(
                      "assets/images/home.png",
                      width: 24,
                      height: 24,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ],
            )
          : null,
      body: Stack(
        alignment: Alignment.center,
        children: [
          // Splash Screen (Logo)
          if (showSplashScreen)
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  // Path gambar background splash screen
                  image: AssetImage("assets/images/bg1.jpg"),
                  fit: BoxFit.cover,
                ),
              ),
              child: Image.asset(
                "assets/images/welcome.png", // Path gambar logo splash screen
                width: 1500, // Ubah ukuran gambar sesuai kebutuhan Anda
                height: 780, // Ubah ukuran gambar sesuai kebutuhan Anda
                fit: BoxFit.contain,
              ),
            ),
          // Tampilan Login
          if (!showSplashScreen)
            Container(
              decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.7),
                borderRadius: BorderRadius.circular(10),
                image: DecorationImage(
                  // Path gambar latar belakang tampilan login
                  image: AssetImage("assets/images/bg1.jpg"),
                  fit: BoxFit.cover,
                ),
              ),
              padding: EdgeInsets.all(20),
              width: double.infinity, // Menggunakan lebar layar penuh
              height: double.infinity, // Menggunakan tinggi layar penuh
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Welcome Back",
                    style: GoogleFonts.inder(
                      color: Colors.black,
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    "Hello there, login to continue",
                    style: GoogleFonts.inriaSans(
                      color: Colors.black,
                      fontSize: 20,
                    ),
                  ),
                  Container(
                    width: 400,
                    height: 100,
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Email',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                          ),
                        ),
                        SizedBox(height: 5),
                        TextField(
                          controller: nameController,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: 400,
                    height: 100,
                    padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Password',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                          ),
                        ),
                        SizedBox(height: 5),
                        TextField(
                          obscureText: true,
                          controller: passwordController,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 50,
                    width: 400,
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Color(0xFF552D0A),
                      ),
                      child: const Text(
                        'Submit',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      onPressed: () {
                        if (nameController.text == 'mark@gmail.com' &&
                            passwordController.text == 'marklee') {
                          userEmail = nameController.text;
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  HomePage(userEmail: userEmail),
                            ),
                          );
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              content: Text('Login failed. Please try again.'),
                            ),
                          );
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
        ],
      ),
    );
  }
}
