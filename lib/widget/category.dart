import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Category extends StatelessWidget {
  final String imagePath;
  final String title;
  final Color borderColor; // Tambahkan properti borderColor
  final Color backgroundColor; // Tambahkan properti backgroundColor

  Category({
    required this.imagePath,
    required this.title,
    required this.borderColor,
    required this.backgroundColor,
    required TextBaseline textBaseline,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.symmetric(
        horizontal: 16.0,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        border: Border.all(
          color: borderColor, // Gunakan borderColor yang diberikan
          width: 2.0,
        ),
        color: backgroundColor, // Gunakan backgroundColor yang diberikan
      ),
      child: Column(
        children: [
          ColorFiltered(
            colorFilter: ColorFilter.mode(
              Colors
                  .transparent, // Tetapkan warna latar belakang gambar ke transparan
              BlendMode
                  .color, // Mode pencampuran warna agar gambar tidak berubah
            ),
            child: Image.asset(
              imagePath,
              width: 50,
              height: 50,
            ),
          ),
          SizedBox(height: 8),
          Text(
            title,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
